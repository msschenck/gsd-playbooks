[CmdletBinding()]
Param(
    [string]$loginName,
    [string]$domain,
    [string]$instanceName
)

Import-Module "sqlps" -DisableNamechecking

try
{
    $newUser = $domain, $loginName -join '\'
    $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $instanceName

    if ($server.Logins.Contains($newUser))
    {
        Write-Host("'$newUser' already exists.")
        $login = $server.Logins | where {$_.Name -eq $newUser}
    }
    else
    {
        $login = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Login -ArgumentList $server, $newUser
        $login.LoginType = [Microsoft.SqlServer.Management.Smo.LoginType]::WindowsUser
        $login.Create()
    }
    $login.AddToRole("sysadmin")
    $login.AddToRole("serveradmin")
    $login.Alter()
    Write-Host("SQL Server login '$loginName' created successfully on $instanceName.")
}
Catch
{
    Write-Error("SQL Server login '$loginName' could not be created on $instanceName - $($_.Exception.Message)")
    throw $_.Exception
}
