[CmdletBinding()]
Param(
    [string]$loginName,
    [string]$password,
    [string]$saPassword,
    [string]$instanceName,
    [string]$newInstanceName = $null
)

Import-Module "sqlps" -DisableNamechecking

try
{
    if(!$newInstanceName)
    {
      $newInstanceName = $instanceName
    }
    # Enable the TCP protocol on the default instance.
    Write-Host 'Enabling TCP for SQL Server.'
    $smo = 'Microsoft.SqlServer.Management.Smo.'
    $uri = "ManagedComputer[@Name='" + (get-item env:\\computername).Value + "']/ServerInstance[@Name='MSSQLSERVER']/ServerProtocol[@Name='Tcp']"
    $wmi = new-object ($smo + 'Wmi.ManagedComputer').
    $Tcp = $wmi.GetSmoObject($uri)
    $Tcp.IsEnabled = $true
    $Tcp.Alter()
    Write-Host 'TCP enabled for SQL Server successfully.'

    # Enable the named pipes protocol for the default instance.
    Write-Host 'Enabling Named Pipes for SQL Server.'
    $uri = "ManagedComputer[@Name='" + (get-item env:\\computername).Value + "']/ServerInstance[@Name='MSSQLSERVER']/ServerProtocol[@Name='Np']"
    $Np = $wmi.GetSmoObject($uri)
    $Np.IsEnabled = $true
    $Np.Alter()
    Write-Host 'Named Pipes enabled for SQL Server successfully.'

}
Catch
{
    Write-Error("An error occurred while trying to configure SQL Server on $instanceName - $($_.Exception.Message)")
    throw $_.Exception
}

try
{
    $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $instanceName

    # turn off windows authentication only mode
    Write-Host 'Turning on SQL mixed authentication mode'
    $server.Settings.LoginMode = [Microsoft.SqlServer.Management.SMO.ServerLoginMode]::Mixed
    Write-Host 'SQL mixed authentication mode set Successfully (a restart of MSSQL is required).'
    $server.Alter()
    # drop login if it exists
    if ($server.Logins.Contains($loginName))
    {
        Write-Host("Deleting the existing login '$loginName' for idempotency.")
        try
        {
          $server.Logins[$loginName].Drop()
        }
        Catch
        {
            # optimistically assume success
            return
        }
    }
    $login = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Login -ArgumentList $server, $loginName

    $login.LoginType = [Microsoft.SqlServer.Management.Smo.LoginType]::SqlLogin
    $login.PasswordExpirationEnabled = $false
    $login.PasswordPolicyEnforced = $false
    $login.Create($password)
    $login.AddToRole("sysadmin")
    $login.AddToRole("serveradmin")
    $login.Alter()
    Write-Host("SQL Server login '$loginName' created successfully on $instanceName.")
}
Catch
{
    Write-Error("SQL Server login '$loginName' could not be created on $instanceName - $($_.Exception.Message)")
    throw $_.Exception
}

try
{
  $repairSql = @"
      DECLARE @oldname nvarchar(MAX)
      SELECT @oldname = @@SERVERNAME
      IF (@oldname IS NULL AND EXISTS (SELECT 1 FROM sys.servers WHERE name = '$newInstanceName'))
          SET @oldname = '$newInstanceName'
      IF (@oldname IS NOT NULL)
          EXEC sp_dropserver @oldname
      EXEC sp_addserver '$newInstanceName', 'local'
      EXEC sp_serveroption @server = '$newInstanceName' ,@optname = 'DATA ACCESS' ,@optvalue = 'TRUE'
"@
  Write-Host 'Executing SQL to repair server name so Apprenda validation passes.'
  Invoke-Sqlcmd -Query $repairSql -ServerInstance "$instanceName"
  Write-Host 'Repair SQL executed successfully.'
}
Catch
{
    Write-Error("Error executing repair SQL for Apprenda validation on $instanceName - $($_.Exception.Message)")
    throw $_.Exception
}

try
{
  $sqlEnableSql = @"
      USE [master]
      GO
      ALTER LOGIN [sa] WITH PASSWORD=N'$saPassword'
      GO
      ALTER LOGIN [sa] ENABLE
      GO
"@
  Write-Host 'Executing SQL to enable sa user and set initial password'
  Invoke-Sqlcmd -Query $sqlEnableSql -ServerInstance "$instanceName"
  Write-Host 'SQL executed successfully'
}
Catch
{
    Write-Error("Error executing SQL to enable sa user and set initial password on $instanceName - $($_.Exception.Message)")
    throw $_.Exception
}

try
{
  Write-Host 'Restarting SQL Server Windows service.'
  Restart-Service -Force MSSQLSERVER
  Write-Host 'SQL Server Windows service was restarted successfully.'
}
Catch
{
    Write-Error("SQL Server Windows service could not be restarted on $instanceName - $($_.Exception.Message)")
    throw $_.Exception
}
