
$sqlServerIsoHttp = "https://s3.amazonaws.com/gsd-automation-artifacts/en_sql_server_2012_enterprise_edition_x86_x64_dvd_813294.iso"
$sqlServerIso = "$PSScriptRoot\sql_server_2012_enterprise.iso"


$client = New-Object System.Net.WebClient
$client.DownloadFile($sqlServerIsoHttp, $sqlServerIso)

$mountResult = Mount-DiskImage -ImagePath $sqlServerIso -PassThru
$drive = ($mountResult | Get-Volume).DriveLetter

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

Remove-Item -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation" -Recurse
new-Item -Name "CredentialsDelegation"  -Path "HKLM:\Software\Policies\Microsoft\Windows\" -type Directory
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "AllowFreshCredentials" -Value "1" -Type DWord
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "AllowFreshCredentialsWhenNTLMOnly" -Value "1" -Type DWord
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "ConcatenateDefaults_AllowFresh" -Value "1" -Type DWord
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -Name "ConcatenateDefaults_AllowFreshNTLMOnly" -Value "1" -Type DWord
new-Item -Name "AllowFreshCredentials"  -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -type Directory
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\AllowFreshCredentials\" -Name "1" -Value "wsman/*"
new-Item -Name "AllowFreshCredentialsWhenNTLMOnly"  -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\" -type Directory
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CredentialsDelegation\AllowFreshCredentialsWhenNTLMOnly\" -Name "1" -Value "wsman/*"


$securePassword = "uio789App" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("localhost\Administrator", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername localhost -credential $credential
invoke-command -session $session -scriptblock { start-job -name netInstaller -scriptblock {Install-WindowsFeature -Name NET-Framework-Core -Source 'Windows Update' -Restart:$false -Verbose} }
invoke-command -session $session -command {$results = (wait-job -name netInstaller | Receive-Job);$results}
Remove-PSSession $session

$securePassword = "appr3ndaadm!n" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("cloud\admin", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername localhost -credential $credential
invoke-command -session $session -scriptblock { start-job -name sqlInstaller -scriptblock {D:\setup.exe /ConfigurationFile=C:\Configuration.ini } }
invoke-command -session $session -command {$results = (wait-job -name sqlInstaller | Receive-Job);$results}
Remove-PSSession $session

Dismount-DiskImage -ImagePath $sqlServerIso -PassThru
New-Item c:\cluster_prepared.txt -type file

Exit

