[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$domainName,
    [string]$instanceHostname
)

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock {param() start-job -name ICMP -scriptblock { param() New-NetFirewallRule -DisplayName "Allow inbound ICMPv4" -Direction Inbound -Protocol ICMPv4 -IcmpType 8 -Action Allow 2>&1 | Out-String } } 
invoke-command -session $session -command {$results = (wait-job -name ICMP | receive-job); $results }

Remove-PSSession $session
Exit

