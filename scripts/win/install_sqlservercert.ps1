[CmdletBinding()]
Param(
  [string]$cert_file,
  [string]$cert_pass
)

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$certDef = -Join("c:\", $cert_file)

Write-Host "============================================="
Write-Host $certDef
Write-Host "============================================="

$secPw = "$cert_pass" | ConvertTo-SecureString -AsPlainText -force
Import-PfxCertificate -FilePath C:\"$cert_file" -CertStoreLocation cert:\LocalMachine\My -Password $secPw

$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2; 
$cert.Import($certDef,$cert_pass,[System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]"DefaultKeySet"); 
$CertificateThumbprint = $cert.thumbprint; 

$regPath = "HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQLServer\SuperSocketNetLib"
Set-ItemProperty -Path $regPath -Name "Certificate" -Value $CertificateThumbprint
Set-ItemProperty -Path $regPath -Name "ForceEncryption" -Value "1"

$regPath = "HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQLServer\SuperSocketNetLib"
Set-ItemProperty -Path $regPath -Name "Certificate" -Value $CertificateThumbprint
Set-ItemProperty -Path $regPath -Name "ForceEncryption" -Value "1"
