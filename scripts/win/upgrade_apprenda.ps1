[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$domainName,
    [string]$instanceHostname,
    [string]$cloudfqdn
)

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock {param($cloudfqdn) start-job -name Installer -scriptblock { param($cloudfqdn) & "C:/apprendato_unpacked/Package/Installer/Apprenda.Wizard.exe" Upgrade -autoRepair -user "admin@apprenda.com" -password "password" -url "http://apps.$cloudfqdn" 2>&1 | Out-String }  -ArgumentList $cloudfqdn} -ArgumentList $cloudfqdn

invoke-command -session $session -command { $results = (wait-job -name Installer |Receive-Job); $results }
Remove-PSSession $session
Exit

