[CmdletBinding()]
Param(
  [string]$cert_file,
  [string]$cert_pass
)

$secPw = "$cert_pass" | ConvertTo-SecureString -AsPlainText -force
Import-PfxCertificate -FilePath C:\"$cert_file" -CertStoreLocation cert:\LocalMachine\My -Password $secPw
