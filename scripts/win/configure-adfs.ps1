[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
    [string]
    $adminUser,
    
    [Parameter(Mandatory=$true)]
    [string]
    $domainName,

    [Parameter(Mandatory=$true)]
    [string]
    $adminPass,

    [Parameter(Mandatory=$true)]
    [string]
    $instanceHostname,

    [Parameter(Mandatory=$true)]
    [string]
    $IdentityURL,

    [Parameter(Mandatory=$true)]
    [string]
    $certFile,

    [Parameter(Mandatory=$true)]
    [string]
    $signCertFile,

    [Parameter(Mandatory=$true)]
    [string]
    $certPassword,

    [Parameter(Mandatory=$true)]
    [string]
    $coreDB
)

winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="3072"}'
Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$adfsScriptBlock = {param($IdentityURL, $certFile, $certPassword, $credential, $signCertFile, $coreDB)
    $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2; 
    $cert.Import($certFile,$certPassword,[System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]"DefaultKeySet"); 
    $CertificateThumbprint = $cert.thumbprint; 
    Install-AdfsFarm -CertificateThumbprint $CertificateThumbprint -FederationServiceName "$IdentityURL" -ServiceAccountCredential $credential -SQLConnectionString "Data Source=$coreDB;Integrated Security=True"
    $signCert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2;
    $signCert.Import($signCertFile, $certPassword, [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]"DefaultKeySet");
    $signCertThumb = $signCert.thumbprint;
    Set-ADFSProperties -AutoCertificateRollover $false
    Add-AdfsCertificate -CertificateType Token-Signing -Thumbprint "$signCertThumb"
    Set-AdfsCertificate -CertificateType Token-Signing -Thumbprint "$signCertThumb" -IsPrimary

}

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$Retries = 0
$Installed = $false
while (($Retries -lt 3) -and (!$Installed)) {
    try {
        Invoke-Command -Authentication Credssp -ComputerName $instanceHostname -Credential $credential -Scriptblock $adfsScriptBlock -ArgumentList $IdentityURL, $certFile, $certPassword, $credential, $signCertFile, $coreDB
        $Installed = $true
    }
    catch {
        $Exception = $_
        $Retries++
        if ($Retries -lt 3) {
            Start-Sleep (([math]::pow($Retries, 2)) * 30)
        }
    }
}

if (!$Installed) {
        throw $Exception
}

Exit
