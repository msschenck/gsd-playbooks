[CmdletBinding()]
Param(
  [string]$new_host,
  [string]$username,
  [string]$password,
  [string]$tenant_alias,
  [string]$cloud_url
)

try
{
  $session = (Invoke-RestMethod -uri "$cloud_url/authentication/api/v1/sessions/developer" -body "{""username"" : ""$username"", ""password"" : ""$password"", ""tenantAlias"" : ""$tenant_alias"" }" -contenttype 'application/json' -method post -TimeoutSec 100)
}
catch
{
  Write-Host "Could not authenticate as $username.com via REST API Call: $cloud_url/authentication/api/v1/sessions/developer"
  Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__
  Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
  exit 1
}

$req = @{
  state='Online'
  reason='Bring a new Linux node online'
}
$json = $req | ConvertTo-Json

try
{
  $details = Invoke-RestMethod -Method Put -Body $json -ContentType 'application/json' -Uri "$cloud_url/soc/api/v1/hosts/$new_host/State" -Headers @{"ApprendaSessionToken" = "$($session.apprendaSessionToken)"}
}
catch
{
  Write-Host "Could not post state update via REST API Call $cloud_url/soc/api/v1/hosts/$new_host/State"
  Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__
  Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
  exit 2
}
