[CmdletBinding()]
Param(
[string]$NewDomainDNSName,
[string]$DomainNetBiosName,
[string]$Password
)
Import-Module ADDSDeployment

$secure_string_pwd = convertto-securestring $Password -asplaintext -force

Install-ADDSForest `
-CreateDnsDelegation:$false `
-DatabasePath "C:\Windows\NTDS" `
-DomainMode "Win2012R2" `
-DomainName "$NewDomainDNSName" `
-DomainNetbiosName $DomainNetBiosName `
-ForestMode "Win2012R2" `
-InstallDns:$true `
-LogPath "C:\Windows\NTDS" `
-NoRebootOnCompletion:$false `
-SysvolPath "C:\Windows\SYSVOL" `
-SafeModeAdministratorPassword $secure_string_pwd  `
-Force:$true  `
