#requires - version 2.0
[CmdletBinding()]
Param
    (
    [Parameter(HelpMessage = 'Type Enable or Disable')]
    [ValidateSet('Enable', 'Disable')]
    [string]$WmiMode, 
    [switch]$WhatIf = $false, 
    [switch]$Confirm = $false
    )
Begin {
        $ScriptName = $MyInvocation.MyCommand.ToString()
        $ScriptPath = $MyInvocation.MyCommand.Path
        $Username = $env:USERDOMAIN + "\" + $env:USERNAME
 
        New - EventLog - Source $ScriptName - LogName 'Windows Powershell' - ErrorAction SilentlyContinue
 
        $Message = "Script: " + $ScriptPath + "`nScript User: " + $Username + "`nStarted: " + (Get - Date).toString()
        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
 
        #	Dotsource in the functions you need.
        $CurrentUser = [System.Security.Principal.WindowsIdentity]::GetCurrent()
        $principal = new - object System.Security.principal.windowsprincipal($CurrentUser)

        if ($principal.IsInRole("Administrators") - eq $false) {
            $Message = 'This script must be run as an administrator from an elevated prompt.'
            Write - Error $Message
            Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "101" - EntryType "Error" - Message $Message - ErrorAction SilentlyContinue
            break
            }
            
        Write - Verbose "Setting up winmgmt commands."
        $StandAloneCMD = "& winmgmt -standalonehost"
        Write - Debug "Standalone command`r`n$($StandAloneCMD)"
        $SharedHostCMD = "& winmgmt -sharedhost"
        Write - Debug "Sharedhost command`r`n`$($SharedHostCMD)"
        
        Write - Verbose "Setting up firewall rules"
        $FirewallPortOpening = "& netsh advfirewall firewall add rule name=`"Open TCP 24158 (WmiFixedPort)`" dir=in action=allow protocol=TCP localport=24158"
        Write - Debug "WmiFixedPort rule creation`r`n$($FirewallPortOpening)"
        $FirewallPortClosing = "& netsh advfirewall firewall delete rule name=`"Open TCP 24158 (WmiFixedPort)`" protocol=TCP localport=24158"
        Write - Debug "WmiFixedPort rule deletion`r`n$($FirewallPortClosing)"
        
        Write - Verbose "Getting the list of services that depend on WMI"
        $Dependencies = Get - Service - Name winmgmt - DependentServices | Where - Object {$_.Status - eq 'Running'}
        }
Process {
        switch ($WmiMode) {
            "Enable" {
                if ($WhatIf) {
                    Write - Host "Executing the following command"
                    Write - Host $StandAloneCMD
                    }
                else {
                    if ($Confirm) {
                        $Message = "Setting the Windows Management Instrumentation service to single mode."
                        Write - Verbose $Message
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                        Invoke - Expression - Command $StandAloneCMD
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $StandAloneCMD - ErrorAction SilentlyContinue
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Stopping dependent services"
                    $Dependencies | Format - List - Property Name
                    }
                else {
                    if ($Confirm) {
                        foreach ($Service in $Dependencies) {
                            $Message = "Stopping dependent service $($Service.Name)"
                            Write - Verbose $Message
                            Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                            try {
                                $Service.Stop()
                                $Service.WaitForStatus('Stopped')
                                $Message = "$($Service.Name) stopped."
                                Write - Debug $Message
                                }
                            catch {
                                $Message = $Error[0].Exception.InnerException.Message
                                Write - Verbose $Message
                                Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "101" - EntryType "Error" - Message $Message - ErrorAction SilentlyContinue
                                }
                            }
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Restarting the Windows Management Instrumentation service"
                    }
                else {
                    if ($Confirm) {
                        $Message = "Restarting the Windows Management Instrumentation service"
                        Write - Verbose $Message
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                        Restart - Service - Name winmgmt - Force
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Starting dependent services"
                    $Dependencies | Format - List - Property Name
                    }
                else {
                    if ($Confirm) {
                        foreach ($Service in $Dependencies) {
                            $Message = "Starting dependent service $($Service.Name)"
                            Write - Verbose $Message
                            Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                            try {
                                $Service.Start()
                                $Service.WaitForStatus('Running')
                                $Message = "$($Service.Name) started."
                                Write - Debug $Message
                                }
                            catch {
                                $Message = $Error[0].Exception.InnerException.Message
                                Write - Verbose $Message
                                Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "101" - EntryType "Error" - Message $Message - ErrorAction SilentlyContinue
                                }
                            }
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Executing the following command"
                    Write - Host $FirewallPortOpening
                    }
                else {
                    if ($Confirm) {
                        $Message = "Opening TCP Port 24158 for Single Port WMI calls"
                        Write - Verbose $Message
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                        Invoke - Expression - Command $FirewallPortOpening
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $FirewallPortOpening - ErrorAction SilentlyContinue
                        }
                    }
                }
            "Disable" {
                if ($WhatIf) {
                    Write - Host "Executing the following command"
                    Write - Host $SharedHostCMD
                    }
                else {
                    if ($Confirm) {
                        $Message = "Setting the Windows Management Instrumentation service to shared host."
                        Write - Verbose $Message
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                        Invoke - Expression - Command $SharedHostCMD
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $SharedHostCMD - ErrorAction SilentlyContinue
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Stopping dependent services"
                    $Dependencies | Format - List - Property Name
                    }
                else {
                    if ($Confirm) {
                        foreach ($Service in $Dependencies) {
                            $Message = "Stopping dependent service $($Service.Name)"
                            Write - Verbose $Message
                            Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                            try {
                                $Service.Stop()
                                $Service.WaitForStatus('Stopped')
                                $Message = "$($Service.Name) stopped."
                                Write - Debug $Message
                                }
                            catch {
                                $Message = $Error[0].Exception.InnerException.Message
                                Write - Verbose $Message
                                Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "101" - EntryType "Error" - Message $Message - ErrorAction SilentlyContinue
                                }
                            }
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Restarting the Windows Management Instrumentation service"
                    }
                else {
                    if ($Confirm) {
                        $Message = "Restarting the Windows Management Instrumentation service"
                        Write - Verbose $Message
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                        Restart - Service - Name winmgmt - Force
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Starting dependent services"
                    $Dependencies | Format - List - Property Name
                    }
                else {
                    if ($Confirm) {
                        foreach ($Service in $Dependencies) {
                            $Message = "Starting dependent service $($Service.Name)"
                            Write - Verbose $Message
                            Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                            try {
                                $Service.Start()
                                $Service.WaitForStatus('Running')
                                $Message = "$($Service.Name) started."
                                Write - Debug $Message
                                }
                            catch {
                                $Message = $Error[0].Exception.InnerException.Message
                                Write - Verbose $Message
                                Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "101" - EntryType "Error" - Message $Message - ErrorAction SilentlyContinue
                                }
                            }
                        }
                    }
                if ($WhatIf) {
                    Write - Host "Executing the following command"
                    Write - Host $FirewallPortClosing
                    }
                else {
                    if ($Confirm) {
                        $Message = "Closing TCP Port 24158 to Single Port WMI calls"
                        Write - Verbose $Message
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
                        Invoke - Expression - Command $FirewallPortClosing
                        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $FirewallPortClosing - ErrorAction SilentlyContinue
                        }
                    }
                }
            }
        }
End {
        $Message = "Script: " + $ScriptPath + "`nScript User: " + $Username + "`nFinished: " + (Get - Date).toString()
        Write - EventLog - LogName 'Windows Powershell' - Source $ScriptName - EventID "104" - EntryType "Information" - Message $Message - ErrorAction SilentlyContinue
        }