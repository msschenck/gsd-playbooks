[CmdletBinding()]
Param(
  [string]$service,
  [string]$serviceAccountName,
  [string]$serviceAccountPass,
  [string]$instanceHostname,
  [string]$domainName
)

$svcD=gwmi win32_service -computername $instanceHostname -filter "name='$Service'"

$StopStatus = $svcD.StopService()
If ($StopStatus.ReturnValue -eq "0")
{
  write-host "$instanceHostname -> Service Stopped Successfully"
}
else
{
  write-host "$instanceHostname -> ($StopStatus.ReturnValue) Service Stop Failed"
}

Start-Sleep -s 10

$ChangeStatus = $svcD.change($null,$null,$null,$null,$null,$null,"$domainName\$serviceAccountName",$serviceAccountPass,$null,$null,$null)
If ($ChangeStatus.ReturnValue -eq "0")
{
  write-host "$instanceHostname -> Sucessfully Changed User Name"
}
else
{
  write-host "$instanceHostname -> ($ChangeStatus.ReturnValue) Service Change Failed"
}

Start-Sleep -s 10

$StartStatus = $svcD.StartService()
If ($ChangeStatus.ReturnValue -eq "0")
{
  write-host "$instanceHostname -> Service Started Successfully"
}
else
{
  write-host "$instanceHostname -> ($StartStatus.ReturnValue) Service Start Failed"
  exit 1
}
