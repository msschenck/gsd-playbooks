[CmdletBinding()]
Param(
  [string]$root_ca_file,
  [string]$root_ca_pass
)

$secPw = "$root_ca_pass" | ConvertTo-SecureString -AsPlainText -force
Import-PfxCertificate -FilePath C:\"$root_ca_file" -CertStoreLocation cert:\LocalMachine\AuthRoot -Password $secPw
