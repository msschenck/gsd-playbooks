[CmdletBinding()]
Param(
[string]$s
)

if(!(Test-Connection -Cn $s -BufferSize 16 -Count 1 -ea 0 -quiet))
{
  Write-Host "Failed to connect"
}
else
{
  Write-Host "Successfully connected"
}
