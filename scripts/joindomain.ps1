[CmdletBinding()]
Param(
[string]$NewDomainDNSName,
[string]$Username,
[string]$Password
)

$concat = $NewDomainDNSName+"\"+$Username

$cred = New-Object System.Management.Automation.PsCredential($concat, (ConvertTo-SecureString $Password -AsPlainText -Force))

Add-Computer -DomainName $NewDomainDNSName -Credential $cred  -OUPath "OU=TestGPO,DC=apprendacloud,DC=zone" -Force
