[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$fsIP,
    [string]$clusterPrimary,
    [string]$fsHost
)

$drive="D"
# Format the drive and bring it online
$number = (Get-Disk | Where-Object Size -GT 274GB | Where-Object Size -LT 276GB | Where-Object PartitionStyle -EQ 'RAW').Number
if($number)
{
  Set-Disk -Number $number -IsReadOnly 0
  Set-Disk -Number $number -IsOffline 0
  Initialize-Disk -Number $number
  New-Partition -DiskNumber $number -DriveLetter $drive -UseMaximumSize
  Start-Sleep -Seconds 5
  Format-Volume -DriveLetter $drive -FileSystem NTFS -Force -Confirm:$false
  Start-Sleep -Seconds 5
}


$drive="E"
# Format the drive and bring it online
$number = (Get-Disk | Where-Object Size -GT 249GB | Where-Object Size -LT 251GB | Where-Object PartitionStyle -EQ 'RAW').Number
if($number)
{
  Set-Disk -Number $number -IsReadOnly 0
  Set-Disk -Number $number -IsOffline 0
  Initialize-Disk -Number $number
  New-Partition -DiskNumber $number -DriveLetter $drive -UseMaximumSize
  Start-Sleep -Seconds 5
  Format-Volume -DriveLetter $drive -FileSystem NTFS -Force -Confirm:$false
  Start-Sleep -Seconds 5
}

$drive="F"
# Format the drive and bring it online
$number = (Get-Disk | Where-Object Size -GT 7GB | Where-Object Size -LT 9GB | Where-Object PartitionStyle -EQ 'RAW').Number
if($number)
{
  Set-Disk -Number $number -IsReadOnly 0
  Set-Disk -Number $number -IsOffline 0
  Initialize-Disk -Number $number
  New-Partition -DiskNumber $number -DriveLetter $drive -UseMaximumSize
  Start-Sleep -Seconds 5
  Format-Volume -DriveLetter $drive -FileSystem NTFS -Force -Confirm:$false
  Start-Sleep -Seconds 5
}

$cda = @(
  "Cluster Disk 1",
  "Cluster Disk 2",
  "Cluster Disk 3"
)

foreach($d in $cda) {
  Get-ClusterResource $d | Where-Object { $_.ResourceType.Name -eq "Physical Disk" } | ForEach-Object {
    $resourceName = $_.Name;  
    $resource  = Get-WmiObject MSCluster_Resource -Namespace root/mscluster | Where-Object { $_.Name -eq $resourceName }
    $disk      = Get-WmiObject -Namespace root/mscluster -Query "ASSOCIATORS OF {$resource} WHERE ResultClass=MSCluster_Disk"
    $partition = Get-WmiObject -Namespace root/mscluster -Query "ASSOCIATORS OF {$disk} WHERE ResultClass=MSCluster_DiskPartition"
  
    if(($Partition.TotalSize * 1024 * 1024) -gt 7GB -and ($Partition.TotalSize * 1024 * 1024) -lt 9GB) {
      (Get-ClusterResource $d).Name = "DTC-Disk"
    }
    if(($Partition.TotalSize * 1024 * 1024) -gt 274GB -and ($Partition.TotalSize * 1024 * 1024) -lt 276GB) {
      $disk = Get-ClusterResource $d
      (Get-ClusterResource $d).Name = "DATA-Disk"
    }
    if(($Partition.TotalSize * 1024 * 1024) -gt 249GB -and ($Partition.TotalSize * 1024 * 1024) -lt 251GB) {
      $disk = Get-ClusterResource $d
      (Get-ClusterResource $d).Name = "LOG-Disk"
    }
  }
}


#if ((Get-ClusterResourceDependency -Resource "DTC-Disk").Resource.OwnerNode.Name -NE $clusterPrimary)
#{
#  Move-ClusterGroup "Available Storage"
#}
