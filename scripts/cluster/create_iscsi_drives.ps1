[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$domainName,
    [string]$instanceHostname,
    [string]$environmentName,
    [string]$node1IP,
    [string]$node2IP
)

$ScriptBlock = {
  param([string]$node1IP, [string]$node2IP)
  
  $drive = (Get-Volume | Where-Object Size -GT 9GB | Where-Object Size -LT 10GB).DriveLetter
  $size = 8GB
  $target = "dtc"
  # Create a virtual disk, without formatting
  $vdisk = $drive+":\iscsi\vdisk.vhdx"
  New-IscsiVirtualDisk -Path $vdisk -Size $size -UseFixed -DoNotClearData
  # create a target called "targetName" for the client instance to access
  New-IscsiServerTarget -TargetName $target -InitiatorId @("IPAddress:$node1IP","IPAddress:$node2IP")
  Set-IscsiServerTarget -TargetName $target
  # Map the iscsi target name to the virtual disk
  Add-IscsiVirtualDiskTargetMapping -TargetName $target -Path $vdisk

  $drive = (Get-Volume | Where-Object Size -GT 274GB | Where-Object Size -LT 276GB).DriveLetter
  $size = 250GB
  $target = "log"
  # Create a virtual disk, without formatting
  $vdisk = $drive+":\iscsi\vdisk.vhdx"
  New-IscsiVirtualDisk -Path $vdisk -Size $size -UseFixed -DoNotClearData
  # create a target called "targetName" for the client instance to access
  New-IscsiServerTarget -TargetName $target -InitiatorId @("IPAddress:$node1IP","IPAddress:$node2IP")
  Set-IscsiServerTarget -TargetName $target
  # Map the iscsi target name to the virtual disk
  Add-IscsiVirtualDiskTargetMapping -TargetName $target -Path $vdisk

  $drive = (Get-Volume | Where-Object Size -GT 299GB | Where-Object Size -LT 301GB).DriveLetter
  $size = 275GB
  $target = "data"
  # Create a virtual disk, without formatting
  $vdisk = $drive+":\iscsi\vdisk.vhdx"
  New-IscsiVirtualDisk -Path $vdisk -Size $size -UseFixed -DoNotClearData
  # create a target called "targetName" for the client instance to access
  New-IscsiServerTarget -TargetName $target -InitiatorId @("IPAddress:$node1IP","IPAddress:$node2IP")
  Set-IscsiServerTarget -TargetName $target
  # Map the iscsi target name to the virtual disk
  Add-IscsiVirtualDiskTargetMapping -TargetName $target -Path $vdisk
}

winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="3072"}'
Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock $ScriptBlock -ArgumentList $node1IP,$node2IP

Remove-PSSession $session
Exit
