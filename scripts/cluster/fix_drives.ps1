[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$fsIP,
    [string]$clusterPrimary,
    [string]$fsHost
)

$cda = @(
  "Cluster Disk 1",
  "Cluster Disk 2",
  "Cluster Disk 3"
)

foreach($d in $cda) {
  Get-ClusterResource $d | Where-Object { $_.ResourceType.Name -eq "Physical Disk" } | ForEach-Object {
    $resourceName = $_.Name;  
    $resource  = Get-WmiObject MSCluster_Resource -Namespace root/mscluster | Where-Object { $_.Name -eq $resourceName }
    $disk      = Get-WmiObject -Namespace root/mscluster -Query "ASSOCIATORS OF {$resource} WHERE ResultClass=MSCluster_Disk"
    $partition = Get-WmiObject -Namespace root/mscluster -Query "ASSOCIATORS OF {$disk} WHERE ResultClass=MSCluster_DiskPartition"
  
    if(($Partition.TotalSize * 1024 * 1024) -gt 7GB -and ($Partition.TotalSize * 1024 * 1024) -lt 9GB) {
      (Get-ClusterResource $d).Name = "DTC-Disk"
    }
    if(($Partition.TotalSize * 1024 * 1024) -gt 274GB -and ($Partition.TotalSize * 1024 * 1024) -lt 276GB) {
      $disk = Get-ClusterResource $d
      (Get-ClusterResource $d).Name = "DATA-Disk"
    }
    if(($Partition.TotalSize * 1024 * 1024) -gt 249GB -and ($Partition.TotalSize * 1024 * 1024) -lt 251GB) {
      $disk = Get-ClusterResource $d
      (Get-ClusterResource $d).Name = "LOG-Disk"
    }
  }
}


if ((Get-ClusterResourceDependency -Resource "DTC-Disk").Resource.OwnerNode.Name -ne $clusterPrimary)
{
  Move-ClusterGroup "Available Storage"
}
