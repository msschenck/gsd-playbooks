[CmdletBinding()]
Param(
  [string]$adminUser,
  [string]$adminPass,
  [string]$domainName,
  [string]$instanceHostname,
  [string]$cloudname,
  [string]$dtcname
)

Start-Transcript -Path C:\fixparams.ps1.txt -Append

winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="3072"}'

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock {param($cloudname,$addressa,$addressb,$dtcname) start-job -name Installer -scriptblock { param($cloudname,$dtcname) $resource = -Join ("SQL Network Name (", $cloudname, ")"); Get-ClusterResource "$resource" | Set-ClusterParameter RegisterAllProvidersIP 0; Get-ClusterResource "$resource" |Set-ClusterParameter HostRecordTTL 60;Stop-ClusterResource "$resource";Start-Sleep -s 10; Start-ClusterResource "$resource"; Start-Sleep -s 10; Start-ClusterGroup "SQL Server (MSSQLSERVER)";Start-Sleep -s 10; Set-DtcNetworkSetting -DtcName $dtcname -AuthenticationLevel NoAuth -InboundTransactionsEnabled 1 -OutboundTransactionsEnabled 1 -XATransactionsEnabled  1 -RemoteAdministrationAccessEnabled 1 -RemoteClientAccessEnabled 1 -LUTransactionsEnabled 1 -confirm:$false }  -ArgumentList $cloudname,$dtcname} -ArgumentList $cloudname,$dtcname
invoke-command -session $session -command {$results = (wait-job -name Installer | receive-job); $results }

Remove-PSSession $session
Exit


