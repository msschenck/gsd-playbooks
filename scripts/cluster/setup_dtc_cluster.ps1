[CmdletBinding()]
Param(
  [string]$adminUser,
  [string]$adminPass,
  [string]$domainName,
  [string]$instanceHostname,
  [string]$cloudname,
  [string]$addressa,
  [string]$addressb
)

winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="3072"}'

Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock {param($cloudname,$addressa,$addressb) start-job -name Installer -scriptblock { param($cloudname,$addressa,$addressb) $dtcrolename=$cloudname+"dtc";$dtcresourcename="MSDTC-"+$cloudname+"dtc";Add-ClusterServerRole -Name $dtcrolename -Storage "DTC-Disk" -StaticAddress $addressa,$addressb;Get-ClusterGroup $dtcrolename | Add-ClusterResource -Name $dtcresourcename -ResourceType "Distributed Transaction Coordinator";Add-ClusterResourceDependency $dtcresourcename $dtcrolename;Add-ClusterResourceDependency $dtcresourcename "DTC-Disk";Start-ClusterGroup $dtcrolename;$msdtc = (Get-WmiObject -Namespace root/MSCluster -Class MSCluster_ResourceGroup |  Where-Object {$_.name -eq $dtcrolename});$msdtcID = $msdtc.id;New-ItemProperty Registry::HKEY_LOCAL_MACHINE\Cluster\Groups\$msdtcID -Name "GroupType" -Value 103 -PropertyType "DWord";Test-Cluster }  -ArgumentList $cloudname,$addressa,$addressb} -ArgumentList $cloudname,$addressa,$addressb
invoke-command -session $session -command {$results = (wait-job -name Installer | receive-job); $results }

Remove-PSSession $session
Exit




