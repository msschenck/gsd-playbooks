[CmdletBinding()]
Param(
    [string]$key
)

Get-ClusterResource $key | Where-Object { $_.ResourceType.Name -eq "Physical Disk" } | ForEach-Object {
  $resourceName = $_.Name;  
  $resource  = Get-WmiObject MSCluster_Resource -Namespace root/mscluster | Where-Object { $_.Name -eq $resourceName }
  $disk      = Get-WmiObject -Namespace root/mscluster -Query "ASSOCIATORS OF {$resource} WHERE ResultClass=MSCluster_Disk"
  $partition = Get-WmiObject -Namespace root/mscluster -Query "ASSOCIATORS OF {$disk} WHERE ResultClass=MSCluster_DiskPartition"

  if(($Partition.TotalSize * 1024 * 1024) -gt 7GB -and ($Partition.TotalSize * 1024 * 1024) -lt 9GB) {
    $drive = ($Partition.Path).Substring(0,1)
  }
  if(($Partition.TotalSize * 1024 * 1024) -gt 274GB -and ($Partition.TotalSize * 1024 * 1024) -lt 276GB) {
    $drive = ($Partition.Path).Substring(0,1)
  }
  if(($Partition.TotalSize * 1024 * 1024) -gt 249GB -and ($Partition.TotalSize * 1024 * 1024) -lt 251GB) {
    $drive = ($Partition.Path).Substring(0,1)
  }
}

return $drive
