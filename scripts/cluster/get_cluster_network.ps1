[CmdletBinding()]
Param(
    [string]$instanceHostname
)

$network = Get-ClusterNetworkInterface -Name "$instanceHostname - Ethernet"

return $network.network.name
