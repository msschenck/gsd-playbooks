[CmdletBinding()]
Param(
    [string]$adminUser,
    [string]$adminPass,
    [string]$domainName,
    [string]$instanceHostname,
    $Nodes,
    [string]$Address,
    [string]$cloudname
)

winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="3072"}'
Enable-WSManCredSSP -Force -Role client -DelegateComputer *
Enable-WSManCredSSP -Force -Role server

$securePassword = "$adminPass" | ConvertTo-SecureString -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential("$domainName\$adminUser", $securePassword)

$session = New-PSSession -Authentication CredSSP -computername $instanceHostname -credential $credential
invoke-command -session $session -scriptblock {param($cloudname, $Nodes, $Address) New-Cluster -Name $cloudname -Node $Nodes -StaticAddress $Address; Test-Cluster} -ArgumentList $cloudname, $Nodes, $Address

Remove-PSSession $session
Exit




