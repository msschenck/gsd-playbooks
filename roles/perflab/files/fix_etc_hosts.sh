#!/usr/bin/env bash

host_name=$(hostname)
grep "${host_name}" /etc/hosts > /dev/null
if [ 0 -ne $? ]
then
  cat << EOF > /etc/hosts
127.0.0.1 localhost
127.0.0.1 `hostname`
EOF

fi


